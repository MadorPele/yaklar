var myConfig = {
  "title": {
    "text": "Our Corporation",
    "background-color": "#666666"
  },
  "shapes": [{
      "type": "rect",
      "background-color": "#5297b6",
      "height": 50,
      "width": 150,
      "x": 293,
      "y": 75,
      "border-radius": "20px",
      "label": {
        "text": "General Manager",
        "color": "white"
      }
    },
    {
      "type": "rect",
      "background-color": "#89ab57",
      "height": 50,
      "width": 150,
      "x": 180,
      "y": 195,
      "border-radius": "20px",
      "label": {
        "text": "Finance",
        "color": "white"
      }
    },
    {
      "type": "rect",
      "background-color": "#89ab57",
      "height": 50,
      "width": 150,
      "x": 410,
      "y": 195,
      "border-radius": "20px",
      "label": {
        "text": "Human Resources",
        "color": "white"
      }
    },
    {
      "type": "rect",
      "background-color": "#666666",
      "height": 50,
      "width": 140,
      "x": 75,
      "y": 330,
      "border-radius": "20px",
      "label": {
        "text": "Research &<br/>Development",
        "color": "white"
      }
    },
    {
      "type": "rect",
      "background-color": "#666666",
      "height": 50,
      "width": 140,
      "x": 285,
      "y": 330,
      "border-radius": "20px",
      "label": {
        "text": "Operations",
        "color": "white"
      }
    },
    {
      "type": "rect",
      "background-color": "#666666",
      "height": 50,
      "width": 140,
      "x": 490,
      "y": 330,
      "border-radius": "20px",
      "label": {
        "text": "Product Marketing",
        "color": "white"
      }
    },
    {
      "type": "line",
      "line-width": "2",
      "line-color": "#999",
      "points": [
        [300, 100],
        [300, 300]
      ]
    },
    {
      "type": "line",
      "line-width": "2",
      "line-color": "#999",
      "points": [
        [250, 200],
        [340, 200]
      ]
    },
    {
      "type": "line",
      "line-width": "2",
      "line-color": "#999",
      "points": [
        [75, 270],
        [500, 270]
      ]
    },
    {
      "type": "line",
      "line-width": "2",
      "line-color": "#999",
      "points": [
        [75, 270],
        [75, 300]
      ]
    },
    {
      "type": "line",
      "line-width": "2",
      "line-color": "#999",
      "points": [
        [500, 270],
        [500, 300]
      ]
    }
  ]
};
$(function(){
  
  zingchart.render({
    id: 'myChart',
    data: myConfig,
    height: "100%",
    width: "100%"
  });
      });
    