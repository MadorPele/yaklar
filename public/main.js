var bool1, bool2, bool3 = false;
// var pakarToRashotBoolRight = false
// var pakarToRashotBoolLeft = false
var pakarToRashotRightCount = 0;
var pakarToRashotLeftCount = 0;
var addLink = 0;
var zrimatMidaCount = 0;
var how = 0;
var love = 0;
var myConfig = {
    pakarToRashot1: "ברשות המקומית טמון מידע רב אודות התכנון העירוני של מרחב הרשות, מידע פרטני אודות מבנים, קווי חשמל ביוב וכו’.<br> בנוסף, קיימים ברשות נתוני מיגון של המבנים בשטחה.",
    pakarToRashot2: "הרשות מחזיקה מידע רב אודות פניות אזרחים לרשות במוקד המידע, אגפי החינוך המקומיים, מחלקות הרווחה והסיוע ושינויים בהרגלי צריכת החשמל והמים ",
    pakarToRashot3: "הרשות מבצעת מעקב אודות אירועי חילוץ, נפילות וכו’.<br> המידע הנאסף ברשויות מהווה תמונה משלימה לקיום האירוע.",
    pakarToRashot4: 'במצב של פיוני אוכלוסייה מהרשות המקומית, הרשות מבצע מעקב של האוכלוסייה במרחב. <br>המידע מתייחס הן למתפנים עצמאית והן לפונים המסתעיים בכוחות פקע"ר והרשו"מ.',
    pakarToRashot5: 'יחד עם העברת מדיניות ההתגוננות, היקל"ר יכול לסייע לרשות להבין את משמעויות מדיניות ההתגוננות והנחיות לניהול שגרת חיים כפי שנקבעו על ידי הפיקוד.',
    pakarToRashot6: 'תוכנית הטיפול באירוע על ידי הנפה וכוחותיה ופעילות הכוחות במרחב הרשות המקומית.<br> בנוסף מתן מענה למענה לדרישות הסיוע שהועברו מהרשות המקומית ליקל"ר.'
}
var tirgolA = {
    camera: "לחיצה על מצלמות רשותיות: נכון, מצלמות רשותיות מהוות מקור למידע ויזואלי ויכולות להוות אינדיקטור על מצב אתר ההרס במידה והייתה נפילה על מבנה. <br>יכולת מיקוד האירוע למצלמה יתאפשר רק לאחר הבנת מיקום הנפילה.",
    telephoneAlerts: 'נכון חלקית... דיווחי טלפון לרשות מהווים בסיס ליצירת תמונת מצב של אירוע הרס. <br>דיווחי טלפון ירוכזו על ידי הרשות המקומית ודרכם ניתן להתחיל ביצירת תמנ"צ לאירוע נפילה.',
    police: 'נכון, נציג משטרה עומד בקשר מול גורמי שיטור בעיר שמרכזים מידע לגבי פניות אזרחים ומיקומי שיטור בשטח.<br> שוטרים ואזרחים מאפשרים מציאת מיקום של נפילות.',
    natzig: 'נכון, מרכז ההפעלה מרכז את פעילות הרשות המקומית ואת המידע המגיע אליו מאזרחים הפונים אל הרשות.',
    kamanGdod: 'נסו שוב.. קמ"ן הגדוד יוצב בהמשך יחד עם גדוד באתר הרס על ידי מפקדת הנפה. עד להגעת הקמ"ן, הוא לא יוכל לתת מידע נוסף לגבי אתר ההרס. <br> עם זאת, בשלבי ההמשך של הטיפול, קמ"ן גדוד יוכל לתת פרטים על מצב החילוץ.',
    samlilNafa: 'נסו שוב... הנפה מרכזת את תמונת המצב ברמתה אך אין לה מידע קונקרטי על מיקום נפילה מדוייק, לא כל שכן למידע מהשטח. <br>הנפה מסתמכת על מידע המגיע אליו מהיקל"ר אותו היא מרכזת ברמתה.'
}
var flipCardCounter1 = 0,
    flipCardCounter2 = 0,
    flipCardCounter3 = 0,
    flipCardCounter4 = 0,
    flipCardCounter5 = 0;
$(function () {
    //Swap between click(startLomda) <=> click(editFunc)
    $("#startButton").click(startLomda);
})

// Remove Function

function editFunc() {
    $("#logoPakar").css("position", "absolute");
    $("#logoModin").css("position", "absolute");
    $("#logoPakar").animate({
            left: "2%",
            top: "2%",
            height: "10vh",
            width: "5vw"
        }),
        $("#logoModin").animate({
            left: "8%",
            top: "2%",
            height: "10vh",
            width: "4vw"
        })
    $("#titleFirstPage").removeClass("animate__bounceInLeft  animate__delay-3s").addClass(" animate__backOutLeft");

    setTimeout(function () {
        $("#firstPage").hide();
        //Temporary commands
        $("#nextButton5").fadeIn(1000);
        //Change function to the current function
        tirgol1();
    }, 1000);
    $("#startButton").fadeOut().off("click", startLomda);
}

function startLomda(event) {
    $("#logoPakar").css("position", "absolute");
    $("#logoModin").css("position", "absolute");
    $("#logoPakar").animate({
            left: "2%",
            top: "2%",
            height: "10vh",
            width: "5vw"
        }),
        $("#logoModin").animate({
            left: "8%",
            top: "2%",
            height: "10vh",
            width: "4vw"
        })
    $("#titleFirstPage").removeClass("animate__bounceInLeft  animate__delay-3s").addClass(" animate__backOutLeft");
    setTimeout(function () {
        $("#firstPage").hide();
    }, 1000);
    $("#startButton").fadeOut().off("click", startLomda);
    $("#secondPage").css("display", "flex");
    $("#titleSecondPage").fadeIn(1000);
    $("#targetText").fadeIn(3000);
    $("#nextButton2").fadeIn(1000).on("click", nextText);
}

function nextText(event) {
    $("#nextButton2").off("click", nextText);
    $("#titleSecondPage").removeClass("animate__bounceInLeft  animate__delay-1s").addClass(" animate__bounceOutUp");
    setTimeout(function () {
        $("#secondPage").hide();
    }, 1000);
    $("#targetText").removeClass("animate__fadeInRightBig animate__delay-1s").addClass("animate__fadeOutRightBig").fadeOut(1000);
    $("#thirdPage").css("display", "flex");
    $("#titleThirdPage").fadeIn(1000);
    $(".list").fadeIn(1000)
    $("#soldier").fadeIn(1000);
    $("#nextButton3").fadeIn(1000).on("click", tfisaPikuditA);
}

function tfisaPikuditA(event) {
    $("#nextButton3").fadeOut(1000).off("click", tfisaPikuditA);
    $("#titleThirdPage").fadeOut(1000);
    $(".list").fadeOut(1000)
    $("#soldier").fadeOut(1000);
    $("#nextButton4").fadeIn(1000).on("click", tfisaPikuditAA);
    $("#fourthPage").css("display", "flex");
    $("#firstStep").fadeIn(1000)
    $("#map").fadeIn(1000)
    $("#rd").fadeIn(1000)
    $("#ru").fadeIn(1000)
    $("#ld").fadeIn(1000)
    $("#lu").fadeIn(1000)
    $("#mi").fadeIn(1000)
    $("#ird").fadeIn(1000)
    $("#iru").fadeIn(1000)
    $("#ild").fadeIn(1000)
    $("#ilu").fadeIn(1000)
    $("#im").fadeIn(1000)
    $("#wtd").fadeIn(1000)
    $("#there").fadeIn(1000)
    $("#titleFourthPage").fadeIn(1000);
    $("#tfisaPikuditText").fadeIn(1000);
    $("#nextButton4").off("click", tfisaPikuditA);
    $("#nextButton4").on("click", tfisaPikuditAA);
}

function tfisaPikuditAA(event) {
    $("#tfisaPikuditText").removeClass("animate__fadeInRightBig animate__delay-1s");
    $("#tfisaPikuditText").addClass("animate__fadeOutRightBig").fadeOut(1000);
    $("#tfisaPikuditTextA").fadeIn(1000);
    $("#nextButton4").off("click", tfisaPikuditAA);
    $("#nextButton4").on("click", tfisaPikuditAAA);
    $("#there").addClass("animate__fadeOutRightBig").fadeOut(1000)
    $("#wtd").addClass("animate__fadeOutRightBig").fadeOut(1000)
}

function tfisaPikuditAAA(event) {
    $("#nextButton4").off("click", tfisaPikuditAAA);
    $("#nextButton4").on("click", tfisaPikuditB);
    $("#map").addClass("animate__fadeOutRightBig").fadeOut(1000)
    $("#rd").addClass("animate__fadeOutRightBig").fadeOut(1000)
    $("#ru").addClass("animate__fadeOutRightBig").fadeOut(1000)
    $("#ld").addClass("animate__fadeOutRightBig").fadeOut(1000)
    $("#lu").addClass("animate__fadeOutRightBig").fadeOut(1000)
    $("#mi").addClass("animate__fadeOutRightBig").fadeOut(1000)
    $("#ird").addClass("animate__fadeOutRightBig").fadeOut(1000)
    $("#iru").addClass("animate__fadeOutRightBig").fadeOut(1000)
    $("#ild").addClass("animate__fadeOutRightBig").fadeOut(1000)
    $("#ilu").addClass("animate__fadeOutRightBig").fadeOut(1000)
    $("#im").addClass("animate__fadeOutRightBig").fadeOut(1000)
    $("#soldier").fadeIn(1000);
    $("#tfisaPikuditTextA").fadeOut(1000);
    $("#tfisaPikuditTextAA").fadeIn(1000);
}

function tfisaPikuditB(event) {
    //Remove it
    $("#soldier").fadeIn(1000);
    // $("#firstStep").on("click", function (){
    //     $(".pages").fadeOut(1000);
    //     setTimeout(function () {
    //         tfisaPikuditA();
    //     }, 1500);
    // });
    $("#firstStep").css({
        backgroundColor: "green",
        borderColor: "green",
        opacity: "50%"
    })
    $("#secondStep").fadeIn(1000)
    $("#nextButton4").off("click", tfisaPikuditB);
    $("#nextButton4").fadeOut(1000)
    $("#tfisaPikuditTextAA").fadeOut(1000);
    $("#titleFourthPage").removeClass("animate__bounceInLeft animate__delay-1s");
    $("#titleFourthPage").addClass("animate__bounceOutUp")
    $("#fifthPage").fadeIn(1000)
    $("#titleFifthPage").fadeIn(1000)
    $("#MavoLyaklarA").fadeIn(1000)
    $("#nextButton5").on("click", MavoLyaklarA).fadeIn(1000);
}

function MavoLyaklarA(event) {
    $("#nextButton5").off("click", MavoLyaklarA);
    $("#MavoLyaklarA").fadeOut(1000)
    $("#soldier").fadeOut(1000);
    $(".tRight").fadeIn(1000)
    $(".lineR").fadeIn(1000)
    $("#yaklar").fadeIn(1000)
    $("#yaklarText1").fadeIn(1000)
    setTimeout(wobleYaklar, 6000);
    $("#yaklar").on("click", leftTree)
    $("#nextButton5").prop('disabled', true);
}

function wobleYaklar() {
    $("#yaklar").removeClass("animate__backInRight animate__delay-1s");
    // $("#yaklar").addClass("animate__wobble");
    $("#yaklar").effect( "shake", {times:8}, 3000 );
}

function leftTree(event) {
    $("#nextButton5").off("click", MavoLyaklarA);
    $("#yaklar").off("click", leftTree)
    $("#yaklar").css({
        backgroundColor: "#5699ce"
    })
    $("#yaklarText1").fadeOut(1000)
    $(".tLeft").fadeIn(1000)
    $(".lineL").fadeIn(1000)
    $("#yaklarText2").fadeIn(1000)
    $("#nextButton5").prop('disabled', false);
    $("#nextButton5").on("click", MavoLyaklarB)
}

function MavoLyaklarB(event) {
    $("#nextButton5").off("click", MavoLyaklarB)
    $(".tRight").removeClass("animate__backInRight animate__delay-1s");
    $(".tRight").addClass("animate__bounceOutDown");
    $(".tLeft").removeClass("animate__backInRight animate__delay-1s");
    $(".tLeft").addClass("animate__bounceOutDown");
    $(".lineR").removeClass("animate__backInRight animate__delay-1s");
    $(".lineR").addClass("animate__bounceOutDown");
    $(".lineL").removeClass("animate__backInRight animate__delay-1s");
    $(".lineL").addClass("animate__bounceOutDown")
    $("#yaklar").addClass("animate__bounceOutDown");
    $("#yaklarText2").fadeOut(1000)
    $("#yaklarText1").fadeOut(1000)
    $("#mavoLyaklarText").fadeIn(1000)
    $("#soldier").fadeIn(1000);
    $("#nextButton5").on("click", MavoLyaklarC)
}

function MavoLyaklarC(event) {
    $("#secondStep").css({
        backgroundColor: "green",
        borderColor: "green",
        opacity: "50%"
    })
    $("#thirdStep").fadeIn(1000)
    $("#mavoLyaklarText").fadeOut(1000);
    $("#soldier").fadeOut(1000);
    $("#nextButton5").off("click", MavoLyaklarC)
    $("#nextButton5").on("click", MavoLyaklarC1)
    $("#divMavoLyaklarText1").fadeIn(1000);
}

function MavoLyaklarC1(event) {
    $("#nextButton5").off("click", MavoLyaklarC1)
    $("#nextButton5").fadeOut(1000);
    $("#divMavoLyaklarText1").fadeOut(1000);
    $("#mavoLyaklarText2").fadeIn(1000);
    $("#dragDrop").fadeIn(1000)

    $("#drop1").droppable({
        accept: "#drag1",
        drop: function (event, ui) {
            bool1 = true
            $(ui.draggable).appendTo(this)
            $(ui.draggable).draggable("disable")
            $(ui.draggable).css({
                top: "-3%",
                left: "1%",
                backgroundColor: "green",
                borderColor: "green",
                textAlign: "center",
                fontSize: "1.3vw"
            })
        }
    })
    $("#drop2").droppable({
        accept: "#drag2",
        drop: function (event, ui) {
            bool2 = true
            $(ui.draggable).appendTo(this)
            $(ui.draggable).draggable("disable")
            $(ui.draggable).css({
                top: "-3%",
                left: "1%",
                backgroundColor: "green",
                borderColor: "green",
                textAlign: "center",
                fontSize: "1.3vw"
            })
        }
    })
    $("#drop3").droppable({
        accept: "#drag3",
        drop: function (event, ui) {
            bool3 = true
            $(ui.draggable).appendTo(this)
            $(ui.draggable).draggable("disable")
            $(ui.draggable).css({
                top: "-3%",
                left: "1%",
                backgroundColor: "green",
                borderColor: "green",
                textAlign: "center",
                fontSize: "1.3vw"
            })
        }
    })
    $("#drag1").draggable({
        revert: "invalid"
    })
    $("#drag2").draggable({
        revert: "invalid"
    })
    $("#drag3").draggable({
        revert: "invalid"
    })
    myInterval = setInterval(function () {
        if ((bool1 && bool2 && bool3) == true) {
            $("#nextButton5").fadeIn();
            clearInterval(myInterval);
        }
    }, 1000)
    $("#nextButton5").on("click", MavoLyaklarD)
}

function MavoLyaklarD(event) {
    $("#nextButton5").off("click", MavoLyaklarD)
    $("#dragDrop").addClass("animate__animated animate__bounceOutDown");
    $("#mavoLyaklarText2").removeClass("animate__delay-3s animate__bounceOutRight")
    $("#mavoLyaklarText2").addClass("animate__bounceOutRight")
    $("#mavoLyaklarText3").fadeIn(1000)
    for (var i = 1; i < 6; i++) {
        $("#flipCard" + i).fadeIn(1000)
    }
    $("#verticalLine1").fadeIn(1000)
    $("#horizonLine1").fadeIn(1000)
    $("#horizonLine2").fadeIn(1000)
    $("#nextButton5").prop('disabled', true);
    $("#flipCard1").mouseover(function () {
        flipCardCounter1++
        if ((flipCardCounter1 > 0) && (flipCardCounter2 > 0) && (flipCardCounter3 > 0) && (flipCardCounter4 > 0) && (flipCardCounter5 > 0)) {
            $("#nextButton5").prop('disabled', false);
        }
    })
    $("#flipCard2").mouseover(function () {
        flipCardCounter2++
        if ((flipCardCounter1 > 0) && (flipCardCounter2 > 0) && (flipCardCounter3 > 0) && (flipCardCounter4 > 0) && (flipCardCounter5 > 0)) {
            $("#nextButton5").prop('disabled', false);
        }
    })
    $("#flipCard3").mouseover(function () {
        flipCardCounter3++
        if ((flipCardCounter1 > 0) && (flipCardCounter2 > 0) && (flipCardCounter3 > 0) && (flipCardCounter4 > 0) && (flipCardCounter5 > 0)) {
            $("#nextButton5").prop('disabled', false);
        }
    })
    $("#flipCard4").mouseover(function () {
        flipCardCounter4++
        if ((flipCardCounter1 > 0) && (flipCardCounter2 > 0) && (flipCardCounter3 > 0) && (flipCardCounter4 > 0) && (flipCardCounter5 > 0)) {
            $("#nextButton5").prop('disabled', false);
        }
    })
    $("#flipCard5").mouseover(function () {
        flipCardCounter5++
        if ((flipCardCounter1 > 0) && (flipCardCounter2 > 0) && (flipCardCounter3 > 0) && (flipCardCounter4 > 0) && (flipCardCounter5 > 0)) {
            $("#nextButton5").prop('disabled', false);
        }
    })
    $("#nextButton5").on("click", MavoLyaklarE)
}

function MavoLyaklarE(event) {
    $("#nextButton5").prop('disabled', true);
    $("#nextButton5").off("click", MavoLyaklarE)
    $("#mavoLyaklarText3").fadeOut(1000);
    $(".line").fadeOut();
    for (var i = 1; i < 6; i++) {
        $("#flipCard" + i).removeClass("animate__bounceInUp animate__delay-2s")
        $("#flipCard" + i).addClass("animate__bounceOutDown")
    }
    // $("#titleFifthPage").css({
    //     "top": "0%"
    // })
    $("#firstTree").fadeIn(1000)
    $("#nextButton5").off("click", MavoLyaklarE)
    $("#lle").fadeIn(1000)
    $("#lll").fadeIn(1000)
    $("#linfooo").fadeIn(1000)
    $("#village").on("click", function () {
        how++;
        console.log(how);
        $("#village").off("click", function () {})
        $("#firstTree").fadeOut(1000)
        $("#linfooo").fadeOut(1000)
        $("#secondTree").fadeIn(1000)
        $("#linfoo").fadeIn(1000)
    })
    $("#town2").on("click", function () {
        how++;
        console.log(how);
        $("#town2").off("click", function () {})
        $("#secondTree").fadeOut(1000)
        $("#linfoo").fadeOut(1000)
        $("#thirdTree").fadeIn(1000)
        $("#linfo").fadeIn(1000)
    })
    $("#city3").on("click", function () {
        how++
        console.log(how);

        $("#city3").off("click", function () {})
        $("#thirdTree").fadeOut(1000)
        $("#lll").fadeOut(1000)
        $("#lle").fadeOut(1000)
        $("#linfo").fadeOut(1000)
        if (how == 6) {
            $("#nextButton5").prop('disabled', false);
        }

    })
    $("#mavoLyaklarText4").fadeIn(1000);
    $("#down").fadeIn(1000);
    $("#nextButton5").on("click", MavoLyaklarF)
}

function MavoLyaklarF(event) {
    // $("#secondStep").on("click", tfisaPikuditB);
    $("#nextButton5").off("click", MavoLyaklarF)
    $(".trees").fadeOut(1000);
    $("#down").fadeOut(1000)
    $("#titleFifthPage").fadeOut(1000);
    $("#mavoLyaklarText4").fadeOut(1000);
    $("#mavoLyaklarText5").fadeIn(1000);
    $("#soldier").fadeIn(1000);
    $("#titleSixPage").fadeIn(1000);
    $("#nextButton5").on("click", zrimatMida)
}

function zrimatMida(event) {
    $("#nextButton5").prop('disabled', true);
    $("#nextButton5").off("click", zrimatMida)
    $("#mavoLyaklarText5").fadeOut(1000);
    $("#nextButton5").on("click", zrimatMidaA)
    $("#soldier").fadeOut(1000);
    $("#textZrima").fadeIn(1000);
    $("#textZrima1").fadeIn(1000);
    $("#zrimatMidaRight").fadeIn(1000);
    $("#zrimatMidaLeft").fadeIn(1000);
    $("#zrimatMidaLeft").on("click", function () {
        pakarToRashotRightCount++;
        // if (pakarToRashotRightCount % 2 !== 0) {
        $("#pakarToRashot").fadeIn(1000)
        $("#pakarToRashotDiv").fadeOut(1000)
        $("#pakarToRashot").css({
            display: "flex"
        })

        $(".topicPakarToRashot").on("click", pakarToRashotText)
        // } else {
        // $("#pakarToRashot").fadeOut(1000)
        // }
    })
    $("#zrimatMidaRight").on("click", function () {
        pakarToRashotLeftCount++;
        // if (pakarToRashotLeftCount % 2 !== 0) {
        $("#pakarToRashot").fadeOut(1000)
        $("#pakarToRashotDiv").fadeIn(1000)
        $("#pakarToRashotDiv").css({
            display: "flex"
        })

        $(".topicPakarToRashot").on("click", pakarToRashotText)
        // } else {
        // $("#pakarToRashotDiv").fadeOut(1000)
        // }
    })
    var setInter = setInterval(function () {
        if ((pakarToRashotLeftCount > 0) && (pakarToRashotRightCount > 0)) {
            $("#nextButton5").prop('disabled', false);
            clearInterval(setInter)
        }
    }, 500)


}

function pakarToRashotText(event) {
    $("#pakarToRashotBox").fadeIn(1000)
    var tId = this.id
    $("#pakarToRashotText").html(myConfig[tId])
    $("#button1").on("click", function () {
        $("#pakarToRashotBox").fadeOut(1000)
    })
}

function zrimatMidaA(event) {
    $("#nextButton5").fadeIn(1000);
    $("#nextButton5").off("click", zrimatMidaA)
    $("#textZrima").fadeOut(1000);
    $("#textZrima1").fadeOut(1000);
    $("#zrimatMidaRight").fadeOut(1000);
    $("#zrimatMidaLeft").fadeOut(1000);
    $("#pakarToRashotDiv").fadeOut(1000)
    $("#pakarToRashot").fadeOut(1000)
    $("#map2").fadeIn(1000)
    $("#zrimatMidaText1").fadeIn(1000)
    $("#nextButton5").on("click", zrimatMidaB)
    $("#verticalLeft").hide();
}

function zrimatMidaB(event) {
    $("#nextButton5").off("click", zrimatMidaB)
    $("#map2").fadeOut(1000)
    $("#map3").fadeIn(1000)
    $("#zrimatMidaText1").fadeOut(1000)
    $("#zrimatMidaText2").fadeIn(1000)
    $("#nextButton5").on("click", zrimatMidaC)
}

function zrimatMidaC(event) {
    $("#nextButton5").prop('disabled', true);
    $("#nextButton5").off("click", zrimatMidaC)
    $("#map3").fadeOut(1000)
    $("#zrimatMidaText2").fadeOut(1000)
    $("#dragDrop2").fadeIn(1000)
    $("#textDrag").fadeIn(1000)
    $("#button2").on("click", closeBox)
    $("#seconedDrop1").droppable({
        accept: ".nafa",
        drop: function (event, ui) {
            $(ui.draggable).appendTo(this)
            $(ui.draggable).draggable("disable")
            $(ui.draggable).css({
                top: "-7.5%",
                left: "1.5%",
                backgroundColor: "green",
                borderColor: "green",
                textAlign: "center",
                fontSize: "1.3vw"
            })
            $("#dragDiv1").append('<div class="seconedDropable nafa" id="seconedDrag4">נפה</div>')
            $("#seconedDrag4").draggable({
                revert: "invalid"
            })
            $("#boxText").fadeIn(1000)
            $("#button2").fadeIn(1000)
            $("#textBox").html("המידע אודות נפילות ואירועי נזק מהווה אינדיקציה לנפה אודות האירועים במרחב. על ידי העברת המידע המדויק ניתן יהיה להחליט על ויסותי כוחות ושליחת צוותים לאירועים.")
            zrimatMidaCount++;
            if (zrimatMidaCount == 5) {
                $("#nextButton5").prop('disabled', false);
            }
        }
    })
    $("#seconedDrop2").droppable({
        accept: ".nafaGdod",
        drop: function (event, ui) {
            $(ui.draggable).appendTo(this)
            $(ui.draggable).draggable("disable")
            $(ui.draggable).css({
                top: "-7.5%",
                left: "1.5%",
                backgroundColor: "green",
                borderColor: "green",
                textAlign: "center",
                fontSize: "1.3vw"
            })
            $("#dragDiv1").append('<div class="seconedDropable nafaGdod" id="seconedDrag5">נפה וגדוד</div>')
            $("#seconedDrag5").draggable({
                revert: "invalid"
            })
            $("#boxText").fadeIn(1000)
            $("#button2").fadeIn(1000)
            $("#textBox").html('אירועים בטחוניים במרחב משפיעים על יכולת הפעולה של כוחות פקע"ר. נתונים אלה הכרחיים לנפה לשם יצירת תמנ"צ וכן לגדוד במידה ואירועים קורים בקרבתו.')
            zrimatMidaCount++;
            if (zrimatMidaCount == 5) {
                $("#nextButton5").prop('disabled', false);
            }
        }
    })
    $("#seconedDrop3").droppable({
        accept: ".nafa",
        drop: function (event, ui) {
            $(ui.draggable).appendTo(this)
            $(ui.draggable).draggable("disable")
            $(ui.draggable).css({
                top: "-7.5%",
                left: "1.5%",
                backgroundColor: "green",
                borderColor: "green",
                textAlign: "center",
                fontSize: "1.3vw"
            })
            $("#dragDiv1").append('<div class="seconedDropable nafa" id="seconedDrag4">נפה</div>')
            $("#seconedDrag4").draggable({
                revert: "invalid"
            })
            $("#boxText").fadeIn(1000)
            $("#button2").fadeIn(1000)
            $("#textBox").html('פגיעה הרציפות התפקודית   לשם קבלת החלטות בנוגע להגשת סיוע לרשויות מתקשות, הנפה צריכה תמונה אחודה של מצבי הרשויות בשטחה. הגדוד לעומת זאת, עוסק בתא שטח מוגדר ואינו זקוק למידע אודות התמודדות הרשות.')
            zrimatMidaCount++;
            if (zrimatMidaCount == 5) {
                $("#nextButton5").prop('disabled', false);
            }
        }
    })
    $("#seconedDrop4").droppable({
        accept: ".nafaGdod",
        drop: function (event, ui) {
            $(ui.draggable).appendTo(this)
            $(ui.draggable).draggable("disable")
            $(ui.draggable).css({
                top: "-7.5%",
                left: "1.5%",
                backgroundColor: "green",
                borderColor: "green",
                textAlign: "center",
                fontSize: "1.3vw"
            })
            $("#dragDiv1").append('<div class="seconedDropable nafaGdod" id="seconedDrag5">נפה וגדוד</div>')
            $("#seconedDrag5").draggable({
                revert: "invalid"
            })
            $("#boxText").fadeIn(1000)
            $("#button2").fadeIn(1000)
            $("#textBox").html('עומסים בצירים ראשיים   המידע אודות תנועה הצירים הינו בעל השלכות לתנועת אזרחים וכוחות בשטח. המידע רלוונטי הן לנפה והן לגדוד במידה ויש לו צורך בהעברת ציוד וכוחות אל ומאתר הרס')
            zrimatMidaCount++;
            if (zrimatMidaCount == 5) {
                $("#nextButton5").prop('disabled', false);
            }
        }
    })
    $("#seconedDrop5").droppable({
        accept: "#seconedDrag3",
        drop: function (event, ui) {
            $(ui.draggable).appendTo(this)
            $(ui.draggable).draggable("disable")
            $(ui.draggable).css({
                top: "-7.5%",
                left: "1.5%",
                backgroundColor: "green",
                borderColor: "green",
                textAlign: "center",
                fontSize: "1.3vw"
            })
            $("#dragDiv1").append('<div class="seconedDropable nafaGdod" id="seconedDrag6">גדוד</div>')
            $("#seconedDrag6").draggable({
                revert: "invalid"
            })
            $("#boxText").fadeIn(1000)
            $("#button2").fadeIn(1000)
            $("#textBox").html('מידע הנדסי הכרחי לגדוד לשם ביצוע משימתו שכן כך ניתן להבין את אופן קריסת המבנה, מיקומים של נעדרים ומהווה בסיס לחלוקת האתר לשם עבודות חילוץ.')
            zrimatMidaCount++;
            if (zrimatMidaCount == 5) {
                $("#nextButton5").prop('disabled', false);
            }
        }
    })
    $("#seconedDrag1").draggable({
        revert: "invalid",

    })
    $("#seconedDrag2").draggable({
        revert: "invalid",

    })
    $("#seconedDrag3").draggable({
        revert: "invalid"
    })

    function closeBox(event) {
        $("#boxText").fadeOut(1000)
    }
    $("#nextButton5").on("click", MarchotMidaA)
}

function MarchotMidaA(event) {
    $("#nextButton5").prop('disabled', true)
    // $("#thirdStep").on("click", MavoLyaklarF);
    $("#nextButton5").off("click", MarchotMidaA)
    $("#dragDrop2").fadeOut(1000)
    $("#titleSixPage").fadeOut(1000)
    $("#thirdStep").css({
        backgroundColor: "green",
        borderColor: "green",
        opacity: "50%"
    })
    $("#fourthStep").fadeIn(1000)
    $("#titleSevenPage").fadeIn(1000)
    $("#marchotMidaText").fadeIn(1000)
    $("#book").fadeIn(1000)
    $("#textBook").fadeIn(1000)
    $("#driveLink").fadeIn(1000)
    $("#marchotMidaVideo").fadeIn(1000)
    $("#marchotMidaVideo").css({
        display: "flex"
    })
    $("#nextButton5").on("click", parakE)
    $("#a1").on("click", addhon)
    $("#a2").on("click", addhon)
    $("#a3").on("click", addhon)
    $("#a4").on("click", addhon)
}

function addhon(event) {
 addLink++;
 if (addLink == 8){
    $("#nextButton5").prop('disabled', false)
 }
}

function parakE(event) {
    // $("#fourthStep").on("click", MarchotMidaA);
    $("#nextButton5").off("click", parakE)
    $("#titleSevenPage").fadeOut(1000)
    $("#marchotMidaText").fadeOut(1000)
    $("#book").fadeOut(1000)
    $("#textBook").fadeOut(1000)
    $("#driveLink").fadeOut(1000)
    $("#marchotMidaVideo").fadeOut(1000)
    $("#fourthStep").css({
        backgroundColor: "green",
        borderColor: "green",
        opacity: "50%"
    })
    $("#fifthStep").fadeIn(1000)
    $("#titleEightPage").fadeIn(1000)
    $("#parakEText").fadeIn(1000)
    $("#nextButton5").on("click", tirgol1)

}

function tirgol1(event) {
    $(".graphic").fadeIn(1000)
    $("#nextButton5").off("click", tirgol1)
    $("#nextButton5").prop('disabled', true);
    $("#titleEightPage").fadeOut(1000)
    $("#parakEText").removeClass("animate__fadeInRightBig animate__delay-1s");
    $("#parakEText").addClass("animate__backOutRight");
    $("#titleTirgolPage").fadeIn(1000)
    setTimeout(function () {
        $(".tirgolDiv").fadeIn(1000)
    }, 1000)
    $("#tirgolText1").fadeIn(1000)
    $(".tirgolDiv").on("click", tirgolAText)
    $("#nextButton5").on("click", tirgolB1)
}

function tirgolAText(event) {
    $("#pakarToRashotBox").fadeIn(1000)
    var tId = this.id
    if ((tId == "samlilNafa") || (tId == "kamanGdod")) {
        $("#pakarToRashotBox").css({
            backgroundColor: "red",
            borderColor: "red"
        })
        $("#" + tId).css({
            backgroundColor: "red",
            borderColor: "red"
        })
    } else {
        $("#pakarToRashotBox").css({
            backgroundColor: "green",
            borderColor: "green"
        })
        $("#" + tId).css({
            backgroundColor: "green",
            borderColor: "green"
        })
        love++;
        console.log(love);
        if (love == 8) {
            $("#nextButton5").prop('disabled', false);
        }
    }
    $("#pakarToRashotText").html(tirgolA[tId])
    $("#button1").on("click", function () {
        $("#pakarToRashotBox").fadeOut(1000)
    })
}

function tirgolB1(event) {
    $("#nextButton5").off("click", tirgolB1)
    $("#tirgol1").fadeOut(1000)
    $("#tirgolBoxDiv").fadeOut(1000)
    $("#tirgolB1Text").fadeIn(1000)
    $("#nextButton5").on("click", tirgolB)
}

function tirgolB(event) {
    $("#kaman").css({
        "top": "40vh"
    })
    $("#kamanPicture").css({
        "top": "40vh"
    })
    $("#policePicture").css({
        "top": "40vh"
    })
    $("#natzigPicture").css({
        "top": "40vh"
    })
    $("#natzigsPicture").css({
        "top": "40vh"
    })
    $(".tirgolDown").css({
        "top": "56vh"
    })
    $("#orePicture").css({
        "top": "21vh"
    })
    $("#nextButton5").off("click", tirgolB)
    $("#nextButton5").on("click", endLomda)
    $(".tirgolDiv").off("click", tirgolAText)
    $(".tirgolDiv").css({
        backgroundColor: "#5699ce",
        borderColor: "#5699ce"
    })
    $("#titleTirgolBPage").fadeIn(1000)
    $("#answerFlex").fadeIn(1000)
    $("#answerFlex").css({
        display: "flex"
    })
    $("#tirgolB1Text").slideUp(1000)
    $("#tirgolBoxDiv").fadeIn(1000)
    $("#tirgolText2").fadeIn(1000)
    $("#boxText").css({
        backgroundColor: "green"
    })
    $(".answerDiv").draggable({
        revert: "invalid"
    })
    $("#camera").droppable({
        accept: "#hasimatZir",
        drop: function (event, ui) {
            $(ui.draggable).remove()
            $("#tirgolBoxText").fadeIn(1000)
            setTimeout(function () {
                $("#tirgolBoxText").fadeOut(1000)
            }, 3000)
        }
    })
    // $("#natzig").droppable({
    //     accept: "#electric",
    //     drop: function (event, ui) {
    //         $(ui.draggable).remove(),
    //             $("#tirgolBoxText").fadeIn(1000)
    //         setTimeout(function () {
    //             $("#tirgolBoxText").fadeOut(1000)
    //         }, 3000)
    //     }
    // })
    $("#police").droppable({
        accept: "#hasimatZir",
        drop: function (event, ui) {
            console.log('vvvvvv')
            $(ui.draggable).remove(),
            $("#tirgolBoxText").fadeIn(1000)
            setTimeout(function () {
                $("#tirgolBoxText").fadeOut(1000)
            }, 3000)
        }
    })
    // $("#police").droppable({
    //     accept: "#electric",
    //     drop: function (event, ui) {
    //         console.log('k')
    //         $(ui.draggable).remove(),
    //             $("#tirgolBoxText").fadeIn(1000)
    //             console.log('ljgd');
    //         setTimeout(function () {
    //             $("#tirgolBoxText").fadeOut(1000)
    //         }, 3000)
    //     }
    // })
    $("#natzig").droppable({
        accept: "#waterBreak",
        drop: function (event, ui) {
            $(ui.draggable).remove(),
                $("#tirgolBoxText").fadeIn(1000)
            setTimeout(function () {
                $("#tirgolBoxText").fadeOut(1000)
            }, 3000)
        }
    })
    $("#natzigs").droppable({
        accept: "#electric",
        drop: function (event, ui) {
            $(ui.draggable).remove(),
                $("#tirgolBoxText").fadeIn(1000)
            setTimeout(function () {
                $("#tirgolBoxText").fadeOut(1000)
            }, 3000)
        }
    })
    $("#kamanGdod").droppable({
        accept: "#kamotLecodim",
        drop: function (event, ui) {
            $(ui.draggable).remove(),
                $("#tirgolBoxText").fadeIn(1000)
            setTimeout(function () {
                $("#tirgolBoxText").fadeOut(1000)
            }, 3000)
        }
    })
}

function endLomda(event) {
    $("#titleTirgolBPage").fadeOut(1000)
    $("#answerFlex").fadeOut(1000)
    $(".tirgolDiv").fadeOut(1000)
    $("#tirgolB1Text").fadeIn(1000)
    $("#tirgolText2").fadeOut(1000)
    $("#tirgolText2").removeClass("animate__fadeInRightBig animate__delay-1s");
    $("#tirgolText2").addClass("animate__fadeOutDown");
    $("#tirgolBoxDiv").fadeOut(1000)
    document.getElementById("nextButton5").style.display = "none";
    $("#nextButton5").text("סיים את הלומדה").css({
        right: "34.5vw",
        bottom: "0vh",
        height: "8vh",
        width: "16vw"
    })
    $("#nextButton5").on("click", function () {
        // $("#fifthStep").on("click", parakE);
        $("#tirgolB1Text").removeClass("animate__fadeInRightBig animate__delay-1s");
        $("#tirgolB1Text").addClass("animate__backOutRight");
        $("#endLomdaText").fadeIn(1000)
        $("#soldier").fadeIn(1000);
        $("#fifthStep").css({
            backgroundColor: "green",
            borderColor: "green",
            opacity: "50%"
        })
        $("#nextButton5").css({
            bottom: "-1000vh"
        })
    })
}