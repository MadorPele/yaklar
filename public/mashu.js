const O = {
    set(key, value) {
        this[key] = {
            value,
            updatedAt: Date.now()
        }
    },
    global: {
        value: null,
        updatedAt: Date.now()
    },
    setAll(value) {
        this.global.value = value
        this.global.updatedAt = Date.now()
    },
    get(key) {
        if (!this[key]) return undefined
        else if (this.global.updatedAt > this[key].updatedAt) {
            return this.global.value
        } else {
            return this[key].value
        }
    }
}

O.set('a', 1)
console.log(
    O.get('a')
);
O.setAll(100)
console.log(
    O.get('a')
);